package com.tw;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import java.util.ArrayList;
import java.util.List;

@Data
@SuperBuilder(toBuilder = true)
@ToString
@EqualsAndHashCode
public class Library {
    private List<Book> books;

    public Library(List<Book> books) {
        if (books == null) {
            books = new ArrayList<>();
        }
        this.books = books;
    }

    public void addBook(Book book) {
        books.add(book);
    }

    public void removeBook(String isbn) {
        books.removeIf(book -> book.getIsbn().equals(isbn));
    }

    public List<Book> getBooksPublishedAfterYear(int year){
        return books.stream().filter(book -> book.getPublishedYear() > year).toList();
    }

    public List<String> getAuthorsOfBooksPublishedBeforeYear(int year){
        return books.stream().filter(book -> book.getPublishedYear() < year).map(Book::getAuthor).toList();
    }
}
