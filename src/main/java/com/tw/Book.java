package com.tw;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

@Data
@SuperBuilder(toBuilder = true)
@ToString
@EqualsAndHashCode
public class Book implements Serializable {
    private String title;
    private String author;
    private int publishedYear;
    private String isbn;

    public String getBookInfo() {
        return "Title: " + title + ", Author: " + author + ", Year: " + publishedYear + ", ISBN: " + isbn + ".";
    }
}
