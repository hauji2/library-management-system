package com.tw;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LibraryTest {
    @Test
    public void test_addBook_should_size_to_be_two() {
        Book book1 = Book.builder()
                .title("Clean Code")
                .author("Robert C. Martin")
                .publishedYear(2010)
                .isbn("978-7-115")
                .build();

        Book book2 = Book.builder()
                .title("Clean Code2")
                .author("Robert C. Martin")
                .publishedYear(2010)
                .isbn("978-7-115")
                .build();

        List<Book> books = new ArrayList<>();
        books.add(book1);

        Library library = new Library(books);
        library.addBook(book2);

        assertEquals(2, library.getBooks().size());
    }

    @Test
    public void test_removeBook_should_size_to_be_one() {
        Book book1 = Book.builder()
                .title("Clean Code")
                .author("Robert C. Martin")
                .publishedYear(2010)
                .isbn("978-7-115")
                .build();

        Book book2 = Book.builder()
                .title("Clean Code2")
                .author("Robert C. Martin")
                .publishedYear(2010)
                .isbn("978-7-116")
                .build();

        List<Book> books = new ArrayList<>();
        books.add(book1);
        books.add(book2);

        Library library = new Library(books);
        library.removeBook(book2.getIsbn());

        assertEquals(1, library.getBooks().size());
    }

    @Test
    public void test_getBooksPublishedAfterYear_should_return_one_only() {
        Book book1 = Book.builder()
                .title("Clean Code")
                .author("Robert C. Martin")
                .publishedYear(2010)
                .isbn("978-7-115")
                .build();

        Book book2 = Book.builder()
                .title("Clean Code2")
                .author("Robert C. Martin")
                .publishedYear(2222)
                .isbn("978-7-116")
                .build();

        List<Book> books = new ArrayList<>();
        books.add(book1);
        books.add(book2);

        Library library = new Library(books);
        List<Book> result = library.getBooksPublishedAfterYear(2010);

        assertEquals(1, result.size());
    }

    @Test
    public void test_getAuthorsOfBooksPublishedBeforeYear_should_return_one_only() {
        Book book1 = Book.builder()
                .title("Clean Code")
                .author("Robert C. Martin22")
                .publishedYear(2010)
                .isbn("978-7-115")
                .build();

        Book book2 = Book.builder()
                .title("Clean Code2")
                .author("Robert C. Martin")
                .publishedYear(2222)
                .isbn("978-7-116")
                .build();

        List<Book> books = new ArrayList<>();
        books.add(book1);
        books.add(book2);

        Library library = new Library(books);
        List<String> result = library.getAuthorsOfBooksPublishedBeforeYear(2011);

        assertEquals(1, result.size());
        assertEquals(book1.getAuthor(), result.get(0));
    }
}
