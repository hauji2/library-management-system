package com.tw;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BookTest {
    @Test
    public void testGetBookInfo() {
        Book book = Book.builder()
                .title("Clean Code")
                .author("Robert C. Martin")
                .publishedYear(2010)
                .isbn("978-7-115")
                .build();

        String expected = "Title: Clean Code, Author: Robert C. Martin, Year: 2010, ISBN: 978-7-115.";
        String actual = book.getBookInfo();

        assertEquals(expected, actual);
    }
}
